#!/bin/sh

print_help() {
    cat << EOF
xmta.sh v0.0
usage: xmta.sh SERVER
SERVER is the URL of the XMTA server, e.g. https://example.com/xmta/
pass token as environment variable TOKEN
message will be read from stdin
EOF
}

if [ -z "$1" ]; then
    print_help
    exit 1
fi
server=$1

if [ -z "$TOKEN" ]; then
    echo "Error: env var TOKEN not set"
    exit 1
fi
token="$TOKEN"

read message

url="${server}/dev/publish/${token}"

curl -s -S -X 'POST' \
     --data "$message" \
     "$url"
