module gitlab.com/jacksgt/xmta

go 1.13

require (
	github.com/aws/aws-lambda-go v1.14.0
	gosrc.io/xmpp v0.4.0
)
