package main

import (
	"errors"
	"log"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gosrc.io/xmpp"
	"gosrc.io/xmpp/stanza"
)

type TokenMapping map[string]string

var tokenRecipientMapping TokenMapping
var account string
var password string

func connect(account string, password string) (client *xmpp.Client, err error) {
	// extract servername and username
	buf := strings.Split(account, "@")
	if len(buf) < 2 {
		return nil, errors.New("Invalid account format")
	}
	serveraddress := buf[1]

	config := xmpp.Config{
		TransportConfiguration: xmpp.TransportConfiguration{
			Address: serveraddress,
		},
		Jid:        account,
		Credential: xmpp.Password(password),
		//StreamLogger: os.Stdout,
		Insecure: false,
	}

	// generate new xmpp client
	client, err = xmpp.NewClient(config, xmpp.NewRouter(), xmppErrorHandler)
	if err != nil {
		log.Printf("Failed to init: %+v\n", err)
		return nil, err
	}

	// establish connection
	err = client.Connect()
	if err != nil {
		log.Printf("Failed to connect: %+v\n", err)
		return nil, err
	}

	return client, nil
}

func sendMessageToJid(x *xmpp.Client, message string, jid string) (err error) {
	msg := stanza.Message{
		Attrs: stanza.Attrs{
			To: jid,
		},
		Body: message,
	}

	err = x.Send(msg)
	return err
}

func xmppErrorHandler(err error) {
	log.Printf("%s\n", err.Error())
}

// generic handler
func publish(token string, message string) (errorCode int, errorMessage string) {
	if len(message) == 0 {
		log.Printf("(400) Bad Request (empty message)\n")
		return 400, "Bad Request: empty message"
	}

	recipient := tokenRecipientMapping[token]
	if recipient == "" {
		log.Printf("(403) Forbidden (no such token)\n")
		return 403, "Forbidden"
	}

	// connect to server
	x, err := connect(account, password)
	if err != nil {
		log.Printf("(500) Failed to connect to XMPP server: %s\n", err)
		return 500, "Internal Server Error: Failed to connect to XMPP server"
	}
	defer x.Disconnect()

	// send message
	err = sendMessageToJid(x, message, recipient)
	if err != nil {
		log.Printf("(500) Failed to send XMPP message: %s\n", err)
		return 500, "Internal Server Error: Failed to send XMPP message"
	}
	log.Printf("Sucessfully sent XMPP message to %s", recipient)

	return 200, "OK"
}

// AWS specific lambda handler
func LambdaHandler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// get token from URL
	buf := strings.Split(request.Path, "/")
	token := buf[len(buf)-1]

	// extract message from request
	message := request.Body

	// send message
	code, text := publish(token, message)

	// set response
	resp := events.APIGatewayProxyResponse{
		StatusCode:      code,
		IsBase64Encoded: false,
		Body:            text,
	}

	return resp, nil
}

func getTokensFromEnv(envName string) (TokenMapping, error) {
	result := make(TokenMapping)
	raw := os.Getenv(envName)
	buf := strings.Split(raw, ",")

	for _, e := range buf {
		if len(e) <= 0 {
			continue
		}
		b := strings.Split(e, "=")
		if len(b) != 2 {
			return nil, errors.New("Invalid format: " + e)
		}
		result[b[0]] = b[1]
	}

	return result, nil
}

func main() {
	var err error
	// set global variables
	account = os.Getenv("ACCOUNT")
	password = os.Getenv("PASSWORD")
	tokenRecipientMapping, err = getTokensFromEnv("TOKEN_RECIPIENT_MAP")
	if err != nil {
		log.Fatalf("Failed to load config: %s\n", err)
		return
	}

	lambda.Start(LambdaHandler)
}
