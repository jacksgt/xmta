package main

import (
	"os"
	"testing"
)

func TestGetTokensFromEnv(t *testing.T) {
	var mapping TokenMapping
	var err error
	os.Setenv("TEST_MAPPING", "abc123=example@example.com,def456=hello@example.com,")
	mapping, err = getTokensFromEnv("TEST_MAPPING")
	if err != nil {
		t.Errorf("getTokensFromEnv returned unexpected error: %v\n", err)
	}
	if mapping["abc123"] != "example@example.com" ||
		mapping["def456"] != "hello@example.com" {
		t.Errorf("getTokensFromEnv did not return expected mapping: %v\n", mapping)
	}

	os.Setenv("TEST_MAPPING", "abc123:wrong@example.com,def456:hello@example.com")
	mapping, err = getTokensFromEnv("TEST_MAPPING")
	if err == nil {
		t.Errorf("getTokensFromEnv did not return expected format error\n")
	}

	os.Setenv("TEST_MAPPING", "foobar=foo@bar.invalid")
	mapping, err = getTokensFromEnv("TEST_MAPPING")
	if err != nil {
		t.Errorf("getTokensFromEnv returned expected error: %v\n", err)
	}
	if mapping["foobar"] != "foo@bar.invalid" {
		t.Errorf("getTokensFromEnv did not return expected mapping: %v\n", mapping)
	}
}
